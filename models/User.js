const mongoose = require("mongoose");

/**
 * Note that _id is ommitted as it is automatically
 * created by Atlas when we generate a new user entry.
 */
const UserSchema = new mongoose.Schema({
  name: String,
  email: String,
  picture: String
});

module.exports = mongoose.model("User", UserSchema);
