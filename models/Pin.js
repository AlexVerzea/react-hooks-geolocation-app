const mongoose = require("mongoose");

/**
 * timestamps: true : Whenever a new Pin is created in our DB, it will give us a
 * createdAt value for when it was created and an updatedAt value for when it was
 * updated.
 */
const PinSchema = new mongoose.Schema(
  {
    title: String,
    content: String,
    image: String,
    latitude: Number,
    longitude: Number,
    author: { type: mongoose.Schema.ObjectId, ref: "User" },
    comments: [
      {
        text: String,
        createdAt: { type: Date, default: Date.now() },
        author: { type: mongoose.Schema.ObjectId, ref: "User" }
      }
    ]
  },
  { timestamps: true }
);

/**
 * Create the "Pin" model in Atlas.
 */
module.exports = mongoose.model("Pin", PinSchema);
