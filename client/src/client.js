import { useState, useEffect } from "react";
import { GraphQLClient } from "graphql-request";

/**
 * insert-production-url is a dummy url for production.
 */
export const BASE_URL =
  process.env.NODE_ENV === "production"
    ? "<insert-production-url>"
    : "http://localhost:3500/graphql";

/**
 * Custom hook that returns the GraphQL Client.
 */
export const useClient = () => {
  const [idToken, setIdToken] = useState("");

  useEffect(() => {
    const token = window.gapi.auth2
      .getAuthInstance()
      .currentUser.get()
      .getAuthResponse().id_token;
    setIdToken(token);
  }, []);

  return new GraphQLClient(BASE_URL, {
    headers: {
      authorization: idToken
    }
  });
};
