import React from "react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MapIcon from "@material-ui/icons/Map";
import Typography from "@material-ui/core/Typography";
import Signin from "../components/Auth/Signin";
import Signout from "../components/Auth/Signout";
import { unstable_useMediaQuery as useMediaQuery } from "@material-ui/core/useMediaQuery";

const Header = ({ classes }) => {
  const mobileSize = useMediaQuery("(max-width: 650px)");
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          {/* Title / Logo */}
          <div className={classes.grow}>
            <MapIcon className={classes.icon} />
            <Typography
              className={mobileSize ? classes.mobile : ""}
              variant="body1"
              color="inherit"
              noWrap
            >
              RHG
            </Typography>
          </div>

          {/* Current User */}
          <Signin />

          {/* Signout Button */}
          <Signout />
        </Toolbar>
      </AppBar>
    </div>
  );
};

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1,
    display: "flex",
    alignItems: "center"
  },
  icon: {
    marginRight: theme.spacing.unit,
    color: "gold",
    fontSize: 40
  },
  mobile: {
    display: "none"
  }
});

export default withStyles(styles)(Header);
