import React, { useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Context from "../../context";
import { unstable_useMediaQuery as useMediaQuery } from "@material-ui/core/useMediaQuery";

const Signout = ({ classes }) => {
  const { state } = useContext(Context);
  const { currentUser } = state;
  const mobileSize = useMediaQuery("(max-width: 650px)");

  return (
    <div className={classes.grow}>
      <img
        className={classes.picture}
        src={currentUser.picture}
        alt={currentUser.name}
      />
      <Typography
        className={mobileSize ? classes.mobile : ""}
        variant="body1"
        color="inherit"
        noWrap
      >
        {currentUser.name}
      </Typography>
    </div>
  );
};

const styles = theme => ({
  root: {
    cursor: "pointer",
    display: "flex"
  },
  grow: {
    flexGrow: 1,
    display: "flex",
    alignItems: "center"
  },
  picture: {
    height: "32px",
    borderRadius: "90%",
    marginRight: theme.spacing.unit * 2
  },
  mobile: {
    display: "none"
  }
});

export default withStyles(styles)(Signout);
